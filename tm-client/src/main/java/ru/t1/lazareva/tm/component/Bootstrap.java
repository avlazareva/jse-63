package ru.t1.lazareva.tm.component;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.api.service.IListenerService;
import ru.t1.lazareva.tm.api.service.ILoggerService;
import ru.t1.lazareva.tm.event.ConsoleEvent;
import ru.t1.lazareva.tm.exception.system.ArgumentNotSupportedException;
import ru.t1.lazareva.tm.exception.system.CommandNotSupportedException;
import ru.t1.lazareva.tm.listener.AbstractListener;
import ru.t1.lazareva.tm.util.SystemUtil;
import ru.t1.lazareva.tm.util.TerminalUtil;

import javax.annotation.PostConstruct;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

@Component
public final class Bootstrap {

    @Nullable
    @Autowired
    private AbstractListener[] listeners;

    @NotNull
    @Autowired
    private ApplicationEventPublisher publisher;

    @NotNull
    @Autowired
    private IListenerService commandService;

    @NotNull
    @Autowired
    private ILoggerService loggerService;

    @NotNull
    @Autowired
    private FileScanner fileScanner;

    @Nullable
    @Autowired
    private AbstractListener[] abstractListeners;

    @PostConstruct
    private void registryCommands() {
        Arrays.stream(abstractListeners).forEach(commandService::add);
    }

    @SneakyThrows
    private void initPID() {
        @NotNull final String filename = "task-manager.pid";
        @NotNull final String pid = Long.toString(SystemUtil.getPID());
        Files.write(Paths.get(filename), pid.getBytes());
        @NotNull final File file = new File(filename);
        file.deleteOnExit();
    }

    private void prepareStartup() {
        initPID();
        loggerService.info("** WELCOME TO TASK-MANAGER **");
        Runtime.getRuntime().addShutdownHook(new Thread(this::prepareShutdown));
        fileScanner.start();
    }

    private void prepareShutdown() {
        loggerService.info("** TASK-MANAGER IS SHUTTING DOWN **");
        fileScanner.stop();
    }

    private void processArguments(@Nullable final String argument) {
        @Nullable final AbstractListener listener = commandService.getCommandByArgument(argument);
        if (listener == null) throw new ArgumentNotSupportedException(argument);
        publisher.publishEvent(new ConsoleEvent(listener.getName()));
    }

    private boolean processArguments(@Nullable final String[] arguments) {
        if (arguments == null || arguments.length < 1) return false;
        @Nullable final String arg = arguments[0];
        processArguments(arg);
        return true;
    }

    public void processCommand(@Nullable final String command) {
        processCommand(command, true);
    }

    public void processCommand(@Nullable final String command, boolean checkRoles) {
        @Nullable final AbstractListener listener = commandService.getCommandByName(command);
        if (listener == null) throw new CommandNotSupportedException(command);
        publisher.publishEvent(new ConsoleEvent(command));
    }

    private void processCommands() {
        try {
            System.out.println("ENTER COMMAND: ");
            @NotNull final String command = TerminalUtil.nextLine();
            publisher.publishEvent(new ConsoleEvent(command));
            System.out.println("[OK]");
            loggerService.command(command);
        } catch (@NotNull final Exception e) {
            loggerService.error(e);
            System.out.println("[FAIL]");
        }
    }

    public void run(@Nullable final String[] args) {
        if (processArguments(args)) System.exit(0);
        prepareStartup();
        while (!Thread.currentThread().isInterrupted()) processCommands();
    }

}