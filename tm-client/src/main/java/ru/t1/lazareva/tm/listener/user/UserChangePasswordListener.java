package ru.t1.lazareva.tm.listener.user;

import org.jetbrains.annotations.NotNull;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.lazareva.tm.dto.request.UserChangePasswordRequest;
import ru.t1.lazareva.tm.event.ConsoleEvent;
import ru.t1.lazareva.tm.util.TerminalUtil;

@Component
public final class UserChangePasswordListener extends AbstractUserListener {

    @NotNull
    private static final String NAME = "change-user-password";

    @NotNull
    private static final String DESCRIPTION = "change password of current user";

    @NotNull
    @Override
    public String getDescription() {
        return DESCRIPTION;
    }

    @NotNull
    @Override
    public String getName() {
        return NAME;
    }

    @Override
    @EventListener(condition = "@userChangePasswordListener.getName() == #consoleEvent.name")
    public void handler(@NotNull final ConsoleEvent consoleEvent) {
        System.out.println("[USER CHANGE PASSWORD]");
        System.out.println("ENTER NEW PASSWORD:");
        @NotNull final String password = TerminalUtil.nextLine();
        @NotNull final UserChangePasswordRequest request = new UserChangePasswordRequest(getToken());
        request.setPassword(password);
        userEndpoint.changeUserPassword(request);
    }

}