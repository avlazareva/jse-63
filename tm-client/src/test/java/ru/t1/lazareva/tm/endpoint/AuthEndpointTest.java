package ru.t1.lazareva.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.junit.Assert;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import ru.t1.lazareva.tm.api.endpoint.IAuthEndpoint;
import ru.t1.lazareva.tm.api.service.IPropertyService;
import ru.t1.lazareva.tm.dto.request.UserLoginRequest;
import ru.t1.lazareva.tm.dto.request.UserLogoutRequest;
import ru.t1.lazareva.tm.dto.request.UserViewProfileRequest;
import ru.t1.lazareva.tm.dto.response.UserLoginResponse;
import ru.t1.lazareva.tm.dto.response.UserLogoutResponse;
import ru.t1.lazareva.tm.dto.response.UserViewProfileResponse;
import ru.t1.lazareva.tm.marker.SoapCategory;
import ru.t1.lazareva.tm.service.PropertyService;

@Category(SoapCategory.class)
public class AuthEndpointTest {

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final IAuthEndpoint authEndpoint = IAuthEndpoint.newInstance(propertyService.getServerHost(), propertyService.getServerPort());

    @Test
    public void testLogin() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        Assert.assertThrows(Exception.class, () -> authEndpoint.login(
                new UserLoginRequest("", ""))
        );
    }

    @Test
    public void testLogout() {
        @NotNull final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @NotNull final UserLogoutResponse userLogoutResponse = authEndpoint.logout(
                new UserLogoutRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userLogoutResponse);
    }

    @Test
    public void testProfile() {
        @Nullable final UserLoginResponse userLoginResponse = authEndpoint.login(
                new UserLoginRequest("admin", "admin")
        );
        Assert.assertNotNull(userLoginResponse);
        Assert.assertNotNull(userLoginResponse.getToken());
        @Nullable final UserViewProfileResponse userProfileResponse = authEndpoint.profile(
                new UserViewProfileRequest(userLoginResponse.getToken())
        );
        Assert.assertNotNull(userProfileResponse);
        Assert.assertNotNull(userProfileResponse.getUser());
    }

}