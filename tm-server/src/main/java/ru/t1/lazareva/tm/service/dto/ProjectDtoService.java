package ru.t1.lazareva.tm.service.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.lazareva.tm.api.service.dto.IProjectDtoService;
import ru.t1.lazareva.tm.dto.model.ProjectDto;
import ru.t1.lazareva.tm.enumerated.Status;
import ru.t1.lazareva.tm.exception.entity.ProjectNotFoundException;
import ru.t1.lazareva.tm.exception.field.*;
import ru.t1.lazareva.tm.repository.dto.ProjectDtoRepository;

@Service
@NoArgsConstructor
@AllArgsConstructor
public final class ProjectDtoService extends AbstractUserOwnedDtoService<ProjectDto, ProjectDtoRepository> implements IProjectDtoService {

    @NotNull
    @Autowired
    private ProjectDtoRepository repository;

    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto changeProjectStatusById(@Nullable final String userId, @Nullable final String id, @Nullable final Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        @Nullable final ProjectDto project = repository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null)
            project.setStatus(status);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto changeProjectStatusByIndex(@Nullable String userId, @Nullable Integer index, @Nullable Status status) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        @Nullable final ProjectDto project = repository.findByUserIdAndIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        if (status != null)
            project.setStatus(status);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        @NotNull ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        repository.save(project);
        return project;
    }

    @NotNull
    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto create(@Nullable final String userId, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @NotNull ProjectDto project = new ProjectDto();
        project.setUserId(userId);
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto updateById(@Nullable final String userId, @Nullable final String id, @Nullable final String name, @Nullable final String description) throws Exception {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (id == null || id.isEmpty()) throw new IdEmptyException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = repository.findByUserIdAndId(userId, id);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

    @Override
    @SneakyThrows
    @Transactional
    public ProjectDto updateByIndex(@Nullable String userId, @Nullable Integer index, @Nullable String name, @Nullable String description) {
        if (userId == null || userId.isEmpty()) throw new UserIdEmptyException();
        if (index == null) throw new IndexIncorrectException();
        if (name == null || name.isEmpty()) throw new NameEmptyException();
        if (description == null || description.isEmpty()) throw new DescriptionEmptyException();
        @Nullable final ProjectDto project = repository.findByUserIdAndIndex(userId, index);
        if (project == null) throw new ProjectNotFoundException();
        project.setName(name);
        project.setDescription(description);
        repository.save(project);
        return project;
    }

}