package ru.t1.lazareva.tm.repository;

import ru.t1.lazareva.tm.model.Project;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class ProjectRepository {

    private static final ProjectRepository INSTANCE = new ProjectRepository();
    private Map<String, Project> projects = new LinkedHashMap<>();

    {
        add(new Project("ONE"));
        add(new Project("TWO"));
        add(new Project("THREE"));
    }

    public static ProjectRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        add(new Project("New project " + System.currentTimeMillis()));
    }

    public void add(Project project) {
        projects.put(project.getId(), project);
    }

    public void save(Project project) {
        projects.put(project.getId(), project);
    }

    public Collection<Project> findAll() {
        return projects.values();
    }

    public Project findById(final String id) {
        return projects.get(id);
    }

    public void removeById(final String id) {
        projects.remove(id);
    }

}
