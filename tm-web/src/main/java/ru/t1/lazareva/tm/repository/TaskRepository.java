package ru.t1.lazareva.tm.repository;

import ru.t1.lazareva.tm.model.Task;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class TaskRepository {

    private static final TaskRepository INSTANCE = new TaskRepository();
    private Map<String, Task> tasks = new LinkedHashMap<>();

    {
        add(new Task("ONE"));
        add(new Task("TWO"));
        add(new Task("THREE"));
    }

    public static TaskRepository getInstance() {
        return INSTANCE;
    }

    public void create() {
        add(new Task("New task " + System.currentTimeMillis()));
    }

    public void add(Task task) {
        tasks.put(task.getId(), task);
    }

    public void save(Task task) {
        tasks.put(task.getId(), task);
    }

    public Collection<Task> findAll() {
        return tasks.values();
    }

    public Task findById(final String id) {
        return tasks.get(id);
    }

    public void removeById(final String id) {
        tasks.remove(id);
    }

}
