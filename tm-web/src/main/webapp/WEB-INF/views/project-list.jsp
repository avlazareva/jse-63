<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<jsp:include page="../include/_header.jsp"/>

<h3>PROJECT LIST</h3>

<table width="100%" cell-padding="10" border="1" style="border-collapse: collapse;">
    <tr>
        <th width="200" nowrap="nowrap">ID</th>
        <th width="200" align="left" nowrap="nowrap">NAME</th>
        <th width="100%" align="left">DESCRIPTION</th>
        <th width="150" align="center" nowrap="nowrap">STATUS</th>
        <th width="100" align="center" nowrap="nowrap">START</th>
        <th width="100" align="center" nowrap="nowrap">FINISH</th>
        <th width="100" align="center" nowrap="nowrap">EDIT</th>
        <th width="100" align="center" nowrap="nowrap">DELETE</th>
    </tr>

    <c:forEach var="project" items="${projects}">
        <tr>
            <td>
                <c:out value="${project.id}"/>
            </td>
            <td>
                <c:out value="${project.name}"/>
            <td>
                <c:out value="${project.description}"/>
            </td>
            <td>
                <c:out value="${project.status.displayName}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateStart}"/>
            </td>
            <td>
                <fmt:formatDate pattern="dd.MM.yyyy" value="${project.dateFinish}"/>
            </td>
            <td>
                <a href="/project/edit/?id=${project.id}"/>EDIT</a>
            </td>
            <td>
                <a href="/project/delete/?id=${project.id}"/>DELETE</a>
            </td>
        </tr>
    </c:forEach>
</table>

<form action="/project/create" style="margin-top: 20px;">
    <button>CREATE PROJECT</button>
</form>

<jsp:include page="../include/_footer.jsp"/>